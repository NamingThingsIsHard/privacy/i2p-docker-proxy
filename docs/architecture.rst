Architecture
============

.. drawio-image:: _static/i2p-docker-components.xml


Handling DNS requests
---------------------

I2P hosts are either `I2P destinations <I2P-destination>`_ or direct hostnames like zzz.i2p,
however these cannot be translated to IP addresses because I2P works with destinations.
We therefore use a **FakeDNS** that maps an I2P host or destination to a random IP.
That IP is communicated to the proxy which will do a reverse DNS lookup once it receives a given IP.

Proxying to I2P
---------------

Instead of using the SOCKS proxy and wrapping everything in HTTP,
we use I2P's `SAM API <SAM-API>`_.
SAM is a lighter protocol and offically recommended for making apps that communicate with I2P.

We create or reuse a SAM session which have a unique ID and can be reused by the proxy to create
tunnels through which we will forward the traffic.
The reverse is also possible meaning we can create a server tunnel with a name (destination in I2P)
that will allow other I2P users to send data to.
That's basically a listening socket that receives data from I2P.

.. mscgenjs::
    :language: msgenny

    app,
    dns : "FakeDNS",
    iptables,
    pr0xy : "Transparent proxy",
    i2p;

    app => dns : "Request resolution of domain name";
    dns => app : "Fake IP";
    app => iptables : "data to IP";
    iptables => pr0xy : "forward data";
    pr0xy >> dns : "reverse DNS request";
    dns >> pr0xy : "domain name";
    pr0xy => i2p : "Create SAM client tunnel";
    i2p => pr0xy : "Tunnel created";
    pr0xy => i2p : "forward data";
    i2p => pr0xy : "response";
    pr0xy => app : "response";

.. _I2P-destination: https://geti2p.net/spec/common-structures#destination
.. _SAM-API: https://geti2p.net/en/docs/api/samv3
