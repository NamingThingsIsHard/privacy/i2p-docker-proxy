Background
==========

These are the concepts and raison d'être of the app.
Hopefully they will provide enough context.

We will take the usecase of YouTube to show how this... app (?) can be used.

Some useful terms before we start:

**Internet**

The global network we use that is comprised of multiple connected networks.

**Clearnet**

The publicly accessible internet that doesn't require specialized software.
This can mostly be accessed in a normal web browser without any advanced settings.

**Darknet**

Specialized software can create networks atop the internet.
This means that computers across the globe join together over the internet to make new networks.

A simple example of such a network could be a person in Israel, a person in Australia, another in Japan and another in Jamaica.
They are all connected to each other and the world thanks to the internet,
but those specific people form their own network over the internet aka an **overlay network**.


Topologies
----------

Centralisation
^^^^^^^^^^^^^^

Our current internet is mostly built around centralisation.
When browsing youtube, all your requests are sent to Google servers and of course so are all the requests of your peers.

.. drawio-image:: _static/I2P-Peertube.drawio
    :alt: Youtube on the clearnet

Below is what this looks like when your peers make requests.

.. drawio-image:: _static/I2P-Peertube.drawio
    :alt: Centralisation on the clearnet
    :page-index: 1

Federalisation
^^^^^^^^^^^^^^

Besides centralisation, we can have federalisation which means groups of servers allow users to create accounts
on each server and communicate with users of other servers.
E-mail is a federated protocol (user@email-server.test) and with the advent of the ActivityPub_ protocol more can
be exchanged than just simple emails (media, text for blogs, user profiles, etc.).

In our scenario, PeerTube_ fulfills that function.

.. drawio-image:: _static/I2P-Peertube.drawio
    :alt: Peertube on the clearnet
    :page-index: 2

Administrators can create PeerTube instances, connect them to other instances, users can create accounts on those
instances and upload videos there or watch videos from local and remote instances.

.. drawio-image:: _static/I2P-Peertube.drawio
    :alt: Peertube federation
    :page-index: 3

Anonymity and security with I2P
-------------------------------

As you can see in the examples above, whatever you request from a server can be tied to you or your computer easily.
Your IP is always available and if law-enformence or the wrong people come a-knockin', a server or instance operator
can (and will) be compelled to hand over server logs to get your IP and "demask" you.
Of course unsavory people and law-enforcement can host their own servers or get a direct tap on the server you're using
to get all the information they require in realtime.

Imagine being queer in the wrong country and viewing content considered offensive there, being a journalist reporting
on a sensitive topic, being a controversial content creator, activist or consumer of that type of content.
You'd want you communications between yourself a the server or the concerned people to stay private and secure.

Some software will be aware of the benefits that I2P (or other overlay networks provide) and explicitly add support
for those networks.
However, not everybody has the time, hence this project.

.. drawio-image:: _static/I2P-Peertube.drawio
    :alt: Peertube on I2P
    :page-index: 4

In the image above, peertube is assumed to already be using this project.
How the project exactly works is explained in :doc:`architecture`.


.. _ActivityPub: https://en.wikipedia.org/wiki/ActivityPub
.. _Peertube: https://joinpeertube.org/
