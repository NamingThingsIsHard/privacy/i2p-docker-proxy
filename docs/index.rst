.. i2p-docker-proxy documentation master file, created by
   sphinx-quickstart on Thu Dec  3 21:40:00 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to i2p-docker-proxy's documentation!
============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   architecture
   background

`i2p-docker-proxy <https://gitlab.com/NamingThingsIsHard/privacy/i2p-docker-proxy>`_ Is a project to allow running
any program in an environment where all net-traffic is routed through I2P.

At the moment, I simply don't know how to code or configure a VPN myself or whether it's even a viable solution,
so for now, everything is done in Docker.
See the :doc:`architecture` doc for more information.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
