# i2p-docker-proxy

An attempt at making an image that forces any software you put in it to pass through I2P.

This is still a work in progress and a repository to explore the possibilities available.
You can read more about it in the [docs]


# Possibilities

 - [iptables - Transparent proxying a single docker container to another docker container][two-containers]
 - [LD_PRELOAD][LD_PRELOAD] with tunnel creation and proxying

[docs]: https://namingthingsishard.gitlab.io/privacy/i2p-docker-proxy
[two-containers]: https://serverfault.com/questions/662297/transparent-proxying-a-single-docker-container-to-another-docker-container
[LD_PRELOAD]: https://rafalcieslak.wordpress.com/2013/04/02/dynamic-linker-tricks-using-ld_preload-to-cheat-inject-features-and-investigate-programs/
