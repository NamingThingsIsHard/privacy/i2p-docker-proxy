#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

# ENV Vars come from from Dockerfile

# Flush the nat table to have a clean start
#  without docker messing around with that
iptables -t nat -F

# https://websistent.com/linux-iptables-log-everything/
iptables -I INPUT 1 -s $PROXY_SAM_HOST -j NFLOG
iptables -t nat -I INPUT 1 -s $PROXY_SAM_HOST  -j NFLOG
iptables -I OUTPUT 1 -j NFLOG
iptables -t nat -I OUTPUT 1 -j NFLOG

# Make sure traffic to I2P isn't redirected elsewhere
iptables -t nat -A OUTPUT -o eth0 \
    -p tcp --dport $PROXY_SAM_PORT \
    -j ACCEPT

# Allow existing connections e.g for servers
iptables -t nat -A OUTPUT -o eth0 \
    -m state \
    --state RELATED,ESTABLISHED \
    -j ACCEPT

# Redirect all other traffic on eth0 to trans-proxy
iptables -t nat -A OUTPUT -o eth0 \
    -p tcp \
    -j REDIRECT --to-ports $PROXY_PORT

# Reject outgoing DNS requests for now
# We don't want them to leak
iptables -A OUTPUT -o eth0 \
    -p udp -m udp --dport 53 \
    -j DROP

# Set the default DNS nameserver to the localhost
echo "nameserver 127.0.0.1" > /etc/resolv.conf

ulogd -d
tcpdump -i any -w /mount/tcp.dmp &

export PYTHONPATH=/opt/bin
python3 -m trans_proxy.cli \
        --verbose
