import os
import socket
import sys

sess = socket.socket(
    socket.AF_INET, socket.SOCK_STREAM)
I2PD_IP = os.getenv("I2PD_IP", "127.0.0.1")
I2PD_PORT = int(os.getenv("I2PD_PORT", 14444))
connect_tuple = (
    I2PD_IP,
    I2PD_PORT
)
print("target %s:%s" % connect_tuple)
sess.connect(connect_tuple)
host = "stats.i2p"
if len(sys.argv) > 1:
    host = sys.argv[1]


def send(string):
    global sess
    # Make sure there are two newlines at the end
    if string[-1] != '\n':
        string = string + '\n\n'
    elif string[-2] != '\n':
        string = string + '\n'
    print("======SENDING====")
    print(string)

    sess.send(string.encode("utf-8"))

    print("=====RECEIVING====")
    # sys.stdout.write(sess.recv(1000).decode())
    response = ''.encode()
    package_size = 128
    while True:
        recv = sess.recv(package_size)
        l = len(recv)
        response += recv
        # Assume no more data
        if l < package_size:
            break
    sys.stdout.write(response.decode())


send("""CONNECT %(host)s:80 HTTP/1.1
Host: %(host)s""" % dict(host=host))

send("""GET / HTTP/1.1                               
Host: %(host)s
User-Agent: MYOB/6.66 (AN/ON)
""" % dict(host=host))
