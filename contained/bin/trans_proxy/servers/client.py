# i2p-docker-proxy
# Copyright (C) 2019 LoveIsGrief
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import asyncio
import logging
from asyncio.streams import StreamReader, StreamWriter

import i2plib

from trans_proxy.utils import get_original_ip


SESSION_NAME = "trans-proxy-session_client"

async def start_client_tcp_tunnel(
        sam_host,
        sam_port,
        ip_dict,
        host="127.0.0.1", port=1234,
        **kwargs
):
    """
    The transparent proxy that forwards clients to their I2P destinations
    """
    logger = logging.getLogger("servers.client_tcp_tunnel")
    logger.info("Starting client tcp tunnel at %s:%s", host, port)
    server = await asyncio.start_server(ClientTcpTunnel.make(
        sam_host=sam_host,
        sam_port=sam_port,
        ip_dict=ip_dict,
        logger=logger,
    ), host=host, port=port)
    await server.serve_forever()


class ClientTcpTunnel:
    session_made = False

    @classmethod
    def make(
            cls,
            sam_host: str,
            sam_port: int,
            ip_dict: dict,
            logger: logging.Logger,
    ):
        # TODO: this seems really dirty. Maybe functools.partial would be cleaner
        async def handle_connection(reader: StreamReader, writer: StreamWriter):
            log = logging.getLogger("ClientTcpTunnel")
            logger.debug("Created")
            logger.debug("ip_dict: %s", ip_dict)
            # The I2P destination that this tunnel is pointing to
            try:
                destination = ip_dict[
                    get_original_ip(writer.get_extra_info('socket'))
                ]
                logger.debug("Set destination to %s", destination)

                # create a SAM stream session
                sam_address = (sam_host, sam_port)
                # TODO: find a cleaner way of doing this
                if not cls.session_made:
                    try:
                        await i2plib.create_session(SESSION_NAME, sam_address)
                        cls.session_made = True
                    except i2plib.exceptions.DuplicatedId:
                        cls.session_made = True

                # connect to a destination
                i2p_reader, i2p_writer = await i2plib.stream_connect(
                    SESSION_NAME,
                    destination,
                    sam_address=sam_address,
                )

                while data := await reader.read(4098):
                    log.debug("data_received")
                    # write data to a socket
                    i2p_writer.write(data)

                    # asynchronously receive data
                    while i2p_response := await i2p_reader.read(4096):
                        log.debug("responding with i2p data")
                        writer.write(i2p_response)

                # close the connection
                i2p_writer.close()
            except:
                logger.exception("Error while proxying")
            finally:
                log.debug("Closing connection")
                writer.close()

        return handle_connection
