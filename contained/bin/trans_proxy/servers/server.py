# i2p-docker-proxy
# Copyright (C) 2019 LoveIsGrief
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import asyncio
import logging

import i2plib
from i2plib import Destination

from trans_proxy.types import Base64

SESSION_NAME = "trans-proxy-session_server"

async def start_server_tcp_tunnel(
        sam_host: str,
        sam_port: int,
        target_port: int,
        priv_key: Base64 = None,
):
    """
    Starts a server tunnel that accepts requests from I2P
     and forwards them to a given local port

    :param sam_host:
    :param sam_port:
    :param target_port: Where to forward the I2P traffic to
    :param priv_key: Private key of a destination as printed in the logs
    """
    loop = asyncio.get_event_loop()

    destination = None
    if priv_key:
        destination = Destination(priv_key, has_private_key=True)

    logger = logging.getLogger("servers.server_tcp_tunnel")
    logger.info("Starting a server tunnel for %s", target_port)
    tunnel = i2plib.ServerTunnel(
        ("127.0.0.1", target_port),
        destination=destination,
        session_name=SESSION_NAME,
        sam_address=(sam_host, sam_port),
        loop=loop
    )
    await tunnel.run()

    log_destination = destination
    if not log_destination:
        log_destination = tunnel.destination
    logger.info("Started server with destination: %s", log_destination.base32)

    # Log private key for later reuse
    # TODO: Probably this isn't safe and we would provide another option
    if not destination:
        logger.info("Your private key is %s", log_destination.private_key.base64)
    try:
        await tunnel.server_loop
    except KeyboardInterrupt:
        pass
    finally:
        tunnel.stop()
        logger.info("Server tunnel closed")
