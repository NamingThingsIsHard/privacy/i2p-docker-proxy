# i2p-docker-proxy
# Copyright (C) 2019 LoveIsGrief
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import socket
import struct

SO_ORIGINAL_DST = 80
"""
A socket option set by netfilter when packets are redirected in the NAT table
"""


class ReverseDict(dict):
    """
    A dict that stores the reverse of itself in another dict
    """

    def __init__(self, reverse_dict: dict):
        super().__init__()
        self._reverse_dict = reverse_dict

    def __setitem__(self, key, value):
        super(ReverseDict, self).__setitem__(key, value)
        self._reverse_dict[value] = key

    def update(self, other=None, **kvps) -> None:
        super(ReverseDict, self).update(other, **kvps)
        self._reverse_dict.update({
            v: k
            for k, v in self.items()
        })

    def clear(self) -> None:
        super(ReverseDict, self).clear()
        self._reverse_dict.clear()


def get_original_ip(sock: socket.socket) -> str:
    odestdata = sock.getsockopt(socket.SOL_IP, SO_ORIGINAL_DST, 16)
    _, port, a1, a2, a3, a4 = struct.unpack("!HHBBBBxxxxxxxx", odestdata)
    return "%d.%d.%d.%d" % (a1, a2, a3, a4)
