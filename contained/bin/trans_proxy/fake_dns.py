# i2p-docker-proxy
# Copyright (C) 2019 LoveIsGrief
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import argparse
import logging
import os
import re
import signal
from datetime import datetime
from pathlib import Path
from threading import RLock
from time import sleep

from dnslib import dns
from dnslib.server import DNSServer, BaseResolver
from faker import Faker

from trans_proxy.utils import ReverseDict

SERIAL_NO = int((datetime.utcnow() - datetime(1970, 1, 1)).total_seconds())

handler = logging.StreamHandler()
handler.setLevel(logging.INFO)
handler.setFormatter(logging.Formatter('%(asctime)s: %(message)s', datefmt='%H:%M:%S'))

logger = logging.getLogger("fake_dns")
logger.addHandler(handler)
logger.setLevel(logging.INFO)


def validate_fqdn(domain_name):
    """
    Taken from https://stackoverflow.com/questions/2532053/validate-a-hostname-string/33715765#33715765

    :param domain_name:
    :type domain_name: basestring
    :rtype: bool
    """
    if domain_name.endswith('.'):
        domain_name = domain_name[:-1]
    if len(domain_name) < 1 or len(domain_name) > 253:
        return False
    ldh_re = re.compile('^[a-z0-9]([a-z0-9-]{0,61}[a-z0-9])?$',
                        re.IGNORECASE)
    return all(ldh_re.match(x) for x in domain_name.split('.'))


class RandomResolver(BaseResolver):
    """
    Resolves a random, public IP for every unique DNS request.
    """
    _FAKER = Faker(providers=["faker.providers.internet"])
    _FAKER_LOCK = RLock()

    def __init__(self, ip_dict=None):
        if ip_dict is None:
            self.fqdn_dict = {}
        else:
            self.fqdn_dict = ReverseDict(ip_dict)

    def create_fake_entry(self, fqdn):
        ip = self._FAKER.ipv4_public(network=False, address_class=None)
        self.fqdn_dict[fqdn] = ip
        logging.getLogger("RandomResolver").info(
            "New IP for %s: %s",
            fqdn, ip
        )
        return ip

    def get_ip(self, fqdn):
        fqdn = fqdn.strip('.')
        with self._FAKER_LOCK:
            ip = self.fqdn_dict.get(fqdn)
            if not ip:
                ip = self.create_fake_entry(fqdn)
            return ip

    def resolve(self, request, handler):
        """
        Resolves a random, public IP for each unique DNS request

        :type request: dnslib.dns.DNSRecord
        :type handler: dnslib.server.DNSHandler
        :rtype: dnslib.dns.DNSRecord
        """
        reply = request.reply()
        fqdn = str(request.q.get_qname())
        reply.add_answer(dns.RR(
            fqdn, rdata=dns.A(self.get_ip(fqdn))
        ))

        return reply


def main(port: int = 53, ip_dict=None):
    """
    Creates a UDP and TCP DNS server with a FakeResolver

    :param port: On localhost to access DNS requests
    :param ip_dict: A dict into which IP addresses of resolved domains will be stored
    """
    resolver = RandomResolver(ip_dict)

    udp_server = DNSServer(resolver, port=port)
    tcp_server = DNSServer(resolver, port=port, tcp=True)
    servers = [udp_server, tcp_server]

    def stop_servers():
        for _server in servers:
            _server.stop()

    def handle_sig(signum, frame):
        logger.info('pid=%d, got signal: %s, stopping...', os.getpid(), signal.Signals(signum).name)
        stop_servers()
        exit(0)

    signal.signal(signal.SIGTERM | signal.SIGINT, handle_sig)

    logger.info('starting DNS server on port %d', port)
    for server in servers:
        server.start_thread()

    try:
        while udp_server.isAlive():
            sleep(1)
    except KeyboardInterrupt:
        pass
    finally:
        stop_servers()


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)

    parser = argparse.ArgumentParser(
        description="A DNS server that returns fake IPs for requests"
    )
    parser.add_argument("-p", "--port",
                        help="Which port to use to accept DNS requests",
                        type=int,
                        default=53)

    main(**parser.parse_args().__dict__)
