# i2p-docker-proxy
# Copyright (C) 2019 LoveIsGrief
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import asyncio
import multiprocessing
from inspect import isawaitable


class AsyncProcess(multiprocessing.Process):
    """
    Adds the ability to run async targets in new processes
    """

    def run(self) -> None:
        if self._target:
            possible_awaitable = self._target(*self._args, **self._kwargs)
            if isawaitable(possible_awaitable):
                asyncio.run(possible_awaitable)
