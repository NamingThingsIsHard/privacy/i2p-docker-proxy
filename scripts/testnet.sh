#!/bin/bash
# https://web.archive.org/web/20200413111226/http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -uo pipefail
IFS=$'\n\t'

# Starts a given amount of routers and waits for them to dump their router information in the netdb
# Once that's done, they should all know of each other
# TODO: Make sure they can connect to each other


# First param is the number of routers (default is 2)
routerCount=${1:-2}

WAIT_TIME=60
FLOODFILL_COUNT=2

_log(){
  echo "====$1"
}

_execAll(){
  service=$1 ; shift
  serviceCount=$1 ; shift
  for i in $(seq 1 ${serviceCount})
  do
    _log "==$service $i"
    docker-compose exec --index $i $service "$@"
  done
}

_log "Clean everything"
docker-compose down -v --remove-orphans

_log "Start up ${routerCount} router(s) and ${FLOODFILL_COUNT} floodfills"
docker-compose up -d --scale i2pd=${routerCount} --scale=i2pd-floodfill=${FLOODFILL_COUNT}

totalRouterCount=$(( $routerCount + $FLOODFILL_COUNT ))
riCount=0
while [ $riCount != $totalRouterCount ]; do
  _log "Waiting ${WAIT_TIME} seconds out of courtesy"
  sleep ${WAIT_TIME}s

  _log "Gracefully shut them down to store their routerInfo (RI) in the netDb"
  _execAll i2pd-floodfill ${FLOODFILL_COUNT} i2prouter stop
  _execAll i2pd ${routerCount} i2prouter stop

  _log "Starting all routers back up"
  sleep 5s
  docker-compose start i2pd-floodfill
  docker-compose start i2pd

  _log "Contents of netDb (expecting ${totalRouterCount} entries)"
  netDbOutput="$(docker-compose exec i2pd find netDb -name '*.dat')"
  riCount=$(echo "${netDbOutput[@]}" | grep routerInfo | wc -l)
  _log "Found $riCount entries:"
  echo "${netDbOutput[@]}"
done

echo "Done! You have a complete netDb"
