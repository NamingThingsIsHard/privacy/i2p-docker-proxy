#!/bin/bash

# Starts a HTTP server in one of the given services

service=${1:-i2pd}

# Get router URL
docker-compose exec i2pd bash -c 'echo http://$(ip a | grep -oE "172.16.200.[[:alnum:]]+" | head -1):7657'

docker-compose exec $service bash -c '
mkdir -p /tmp/www
cd /tmp/www
echo "This is a server" > index.html
python3 -m http.server 7658
'
